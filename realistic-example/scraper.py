from bs4 import BeautifulSoup
import re
from urllib.parse import urlparse

import web_page
def scrape_emails(url):
    page = web_page.fetch(url)
    if page is not None:
        emails = extract_emails(page)
        nested_urls = extract_urls(page, url)
        return emails, nested_urls

# http://scraping.pro/simple-email-crawler-python/

def extract_emails(text):
    # extract all email addresses and add them into the resulting set
    emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", text, re.I))
    return emails

def extract_urls(text, url):
    # extract base url to resolve relative links
    parts = urlparse(url)
    base_url = "{0.scheme}://{0.netloc}".format(parts)
    path = url[:url.rfind('/')+1] if '/' in parts.path else url

     # create a beutiful soup for the html document
    soup = BeautifulSoup(text, 'html.parser')

    processed_urls = [] # dummy one; to be updated later
    new_urls = []
    # find and process all the anchors in the document
    for anchor in soup.find_all("a"):
        # extract link url from the anchor
        link = anchor.attrs["href"] if "href" in anchor.attrs else ''
        # resolve relative links
        if link.startswith('/'):
            link = base_url + link
        elif not link.startswith('http'):
            link = path + link
        # add the new url to the queue if it was not enqueued nor processed yet
        if not link in new_urls and not link in processed_urls:
            new_urls.append(link)

if __name__ == "__main__":
    import sys
    url = sys.argv[1]
    print(scrape_emails(url))


