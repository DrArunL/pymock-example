
import requests
def fetch(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.content.decode('ISO-8859-1')
    

