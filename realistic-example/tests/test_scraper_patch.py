import scraper

def mock_fetch(url):
    return "<html> axxx@louisiana.edu </html>"

def test_scraper(monkeypatch):
    monkeypatch.setattr('scraper.web_page.fetch', mock_fetch)
    emails, urls = scraper.scrape_emails("http://dataquestio.github.io/web-scraping-pages/simple.html")
    assert emails == set(['axxx@louisiana.edu'])


