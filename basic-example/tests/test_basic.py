# This file contains usual unit test
#
from program import square, main

def test_main():
    assert main(5) == 150

def test_square():
    assert square(5) == 25
    
