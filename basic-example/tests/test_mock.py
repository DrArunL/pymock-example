"""AN EXAMPLE OF USING MOCK TO REPLACE A FUNCTION BY A MOCKED ONE.


"""

## The following try/except code block is used to enable this test to run on both python 2.7 and 3+.
try:
    import mock
except ImportError:
    from unittest import mock

## import the functions to be tested
from program import square, main


# function the functions square and cube inside the module `function`.
@mock.patch('program.square')
@mock.patch('program.cube')
def test_main(mocked_square, mocked_cube):
    # underling function are mocks so calling main(5) will return mock
    mocked_square.return_value = 1
    mocked_cube.return_value = 0
    assert main(5) == 1
    mocked_square.assert_called_once_with(5)
    mocked_cube.assert_called_once_with(5)

@mock.patch('test_mock.square', return_value=1)
def test_square(mocked_square):
    # because you need to patch in exact place where function that has to be mocked is called
    assert square(5) == 1
