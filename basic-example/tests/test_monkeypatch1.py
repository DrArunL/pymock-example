from program import square, main

def test_main(monkeypatch): 
    monkeypatch.setattr('program.square', lambda x: 1) 
    monkeypatch.setattr('program.cube', lambda x: 0) 
    assert main(5) == 1

def test_square(monkeypatch):
    monkeypatch.setattr('test_monkeypatch1.square', lambda x: 1)
    assert square(5) == 1

