from program import square, main

def mock_square(x):
    if x == 1:
        return 3
    if x == 2:
        return 5
    return 1

def test_main(monkeypatch): 
    monkeypatch.setattr('program.square', lambda x: 1) 
    monkeypatch.setattr('program.cube', lambda x: 0) 
    assert main(5) == 1

def test_square(monkeypatch):
    monkeypatch.setattr('test_monkeypatch2.square', mock_square)
    assert square(2) == 5
