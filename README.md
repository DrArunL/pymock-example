# Examples for using mock and monkeypatch in Python

This repo contains the following example programs to demonstrate the use of mocking and monkeypatch in python:

* basic-example
* realistic-example

## Example: basic-example

The example in the `basic-example` directory are influenced by the following blog:

* [Mocks and Monkeypatching in Python](https://semaphoreci.com/community/tutorials/mocks-and-monkeypatching-in-python)

This example contains the following program:

* `program.py` - a program with three functions `square`, `cube`, and `main`. The function `main` calls the functions `square` and `cube`.

The subdirectory `tests` contain the following tests:

* `test_basic.py`: A normal test case without mocking.
* `test_mock.py`: Tests with `cube` and `square` mocked.
* `test_monkeypatch1.py`: Tests with `cube` and `square` patched to return a fixed value.
* `test_monkeypatch2.py`: Tests with `square` patched to `mock_square`.

Each of the test case files have two test cases:

* test_main
* test_square

The two testcases have a very subtle difference. It is important you understand the difference. The difference arises from how python treats an import statement.

Consider the import statement:
```
        from program import square, main
```

It `imports` the functions `square` and `main` inside the specific test case file, such as `test_mock.py`.

The first testcase, `test_main`, calls the function `main`, which in turn the functions `square` and `cube`. The second test case, `test_square` calls the function `square`.

Though both the test cases call the function `square`, turns out that in Python they call two different instances of `square`. The testcase `test_main` calls the instance `program.square` (that is the function in the module `program`). The testcase `test_square` calls the instance of `square` that is imported in the current module, example `test_basic.square`.

### Test: test_basic.py

This test cases shows a simple example of mocking a function.

The decoration `@mock.patch` is used to create a mocked version of a function just for the specific test case it decorates. In the following code fragment mocked versions of the function `program.square` and `program.cube` are used in the test case `test_main`.

```
@mock.patch('program.square')
@mock.patch('program.cube')
def test_main(mocked_square, mocked_cube):
    ...
```

Inside `test_main` you can associate specific behavior with the mocked function. The following states that `mocked_square` should always return the value 1 and `mocked_cube` should return 0.

```
    mocked_square.return_value = 1
    mocked_cube.return_value = 0
```

So when `test_main` calls the function `main(5)` it will produce the result 1+0, instead of 125+25.

The following code confirms that `main` calls `square` and `cube` exactly once with the parameter 5.

```
    mocked_square.assert_called_once_with(5)
    mocked_cube.assert_called_once_with(5)
```

Now let us look at the second test case in `test_mock.py`.

```
@mock.patch('test_mock.square', return_value=1)
def test_square(mocked_square):
    # because you need to patch in exact place where function that has to be mocked is called
    assert square(5) == 1

```

Notice it is mocking `test_mock.square` because when the function `test_square` calls `square` the instance `test_mock.square' is called.

Change it to `program.square` and see if the test still passes or fails.


### Test: test_monkeypatch1.py

Unlike mock, monkeypatch is used to completely replace a function with another function. And the effect is completely local to the test case in which it is called.

```
    monkeypatch.setattr('program.square', lambda x: 1) 
    monkeypatch.setattr('program.cube', lambda x: 0) 
```

The above code in `test_main` patches the function `program.square` with the (lambda) expression `lambda x: 1`, which is a function. It also patches the function `program.cube` to the lambda expression `lambda x: 0`.

Similarly the following code in `test_square` patches the `test_monkeypatch1` instance of `square`:
```
    monkeypatch.setattr('test_monkeypatch1.square', lambda x: 1)
```

### Test: test_monkeypatch2.py

This test goes one step further. In `test_square` testcase it patches the function `square` by `mock_square`. This example shows that patching doesn't require a nameless (lambda) expression. It can use an actual function that can contain complex computation.

## Example: realistic-example

The directory `realistic-example` contains the programs:

* `web_page.py`: contains a function `fetch` to get the page at a given url.
* `scraper.py`: scrapes email addresses and urls from the page at a given url

The test cases are:

* `test_fetch.py`: Test the function `web_page.fetch`
* `test_scraper_nomock.py`: Test the function `scraper.scrape_emails` by actually fetching page from a website.
* `test_scraper_patch.py`: Test the function `scraper.scrape_emails` by using a mock implementation of `fetch`.

## How to run the tests

### Run tests in basic-example
```
    cd basic-example
    python -m pytest tests

```

### Run tests in realistic-example
```
    cd realistic-example
    python -m pytest tests
```
